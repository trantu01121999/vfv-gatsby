module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: ['plugin:jsx-a11y/recommended', 'prettier', 'prettier/react'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['@typescript-eslint', 'react', 'prettier'],
    rules: {
        semi: 0,
        'id-length': [
            'error',
            {
                properties: 'never',
                exceptions: ['e', 'i'],
            },
        ],
        'react/jsx-filename-extension': [
            2,
            {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
            },
        ],
        'prettier/prettier': [
            'error',
            {
                semi: false,
            },
        ],
        'react/prop-types': [0],
    },
    // rules: {
    //     // 'react/prop-types': 0,
    //     "jsx-a11y/anchor-is-valid": [ "error", {
    //         "components": [ "Link" ],
    //         "specialLink": [ "hrefLeft", "hrefRight" ],
    //         "aspects": [ "invalidHref", "preferButton" ]
    //     }]
    // },
}
