export const SLIDE = [1, 2, 3, 4];
export const NEW = [1, 2, 3, 4, 5, 6];
export const YEAR = [2022, 2021, 2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001];
export const TABS = [
  "Tổng quan",
  "Thể thức thi đấu",
  "Các đội tham dự",
  "Lịch thi đấu & kết quả"
];

export const HEADERNAV = [
  {
    key: 1,
    text: "Trang Chủ",
    value: "home",
    link: "/",
  },
  {
    key: 2,
    text: "Giới Thiệu",
    value: "intro",
    link: "/gioi-thieu",
  },
  {
    key: 3,
    text: "Tin Tức",
    value: "news",
    link: "/tin-tuc",
  },
  {
    key: 4,
    text: "Giải Đấu",
    value: "Tournaments",
    link: "/giai-dau",
  },
  {
    key: 5,
    text: "Đội Bóng",
    value: "Teams",
    link: "/doi-bong",
  },
  {
    key: 6,
    text: "Thư Viện Ảnh",
    value: "Library",
    link: "/thu-vien-anh",
  },
  {
    key: 7,
    text: "Giải Đấu Detail",
    value: "Tournaments Detail",
    link: "/giai-dau-detail",
  },
];
