import React from 'react';
import {Link} from "gatsby";
// @ts-ignore
import image1 from "../../assets/image/item-new.png";
import {Tag} from "antd";

const ItemNew = () => {
        return (
            <div className="itemNew">
                <Link
                    to={"/tin-tuc/giai-dau-trong-nuoc"}
                    className={"itemNewContent"}
                >
                    <div className="image">
                        <img src={image1} alt="" />
                    </div>
                    <div className="text">
                        <Tag color="#2F3A4A">Giải đấu trong nước</Tag>
                        <div className="box-bottom">
                            <p className="short-desc">
                                Thắng Ninh Bình Doveco 3-2, Than Quảng Ninh giành
                                hạng 3 giải bóng chuyền VĐQG Cúp Bamboo Airwa...
                            </p>
                            <span className="date">14/01/2022</span>
                        </div>
                    </div>
                </Link>
            </div>
        );

}

export default ItemNew;