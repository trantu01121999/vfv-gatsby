// @ts-ignore
import React, {useEffect, useState} from "react";
import { HEADERNAV } from "../../contants/common";
import { Link } from "gatsby";

// @ts-ignore
import logo from "../../assets/image/LogoLienDoan.png";
// @ts-ignore
import background from "../../assets/image/TopLine-bg.png";
import { Button, Drawer, Space } from "antd";
import { CloseNav, TabNav, Volleyball } from "../../contants/icon";
import { useLocation } from "@reach/router";

const Header = () => {
  const { pathname, search } = useLocation();
  const [scroll, setScroll] = useState(false)
  const [buttonNav, setButtonNav] = useState<string>("home");
  const [openNav, setOpenNav] = useState<boolean>(false);

  const onCloseDrawer = () => {
    setOpenNav(false);
  };

  const handleActive = (value: string) => {
    setButtonNav(value);
  };

  const handleNavMobile = () => {
    setOpenNav(true);
  };

  useEffect(() => {
    window.addEventListener('scroll', () => {
      setScroll(window.scrollY > 50)
    })
  }, [])

  return (
    <header className="header">
      <div className="header-top" style={{ background: `url(${background})` }}>
        <div className="container flexGroup3" style={{ padding: "5px 0" }}>
          <span className="white-1 mr-15">
            <span className="mr-10">Tiếng Việt</span> |{" "}
            <span className="ml-10">English</span>
          </span>
        </div>
      </div>
      <div className={`header-bot ${scroll ? 'scrollHeader' : ''}`}>
        <div className="container flexGroup">
          <div className="dFlex">
            <img className="logo" src={logo} alt="img" />
            <div className="dFlexColumnJustifyCenter">
              <span className="textLogo semibold">
                LIÊN ĐOÀN BÓNG CHUYỀN VIỆT NAM
              </span>
              <span className="textLogo">Volleyball Federation of Vietnam</span>
            </div>
          </div>
          <div className="dFlex header-nav-desktop">
            {HEADERNAV.map((item: any) => {
              return (
                <Link to={item.link} key={item.key}>
                  <Button
                    type="primary"
                    icon={pathname === item.link ? <Volleyball /> : false}
                    className="flexCenter buttonNav"
                    ghost={true}
                  >
                    <span
                      className={`textNav ${
                        pathname === item.link ? "active" : ""
                      }`}
                    >
                      {item.text}
                    </span>
                  </Button>
                </Link>
              );
            })}
          </div>
          <div className="header-nav-mobile">
            <Button
              type="primary"
              className="flexCenter buttonNav"
              ghost={true}
              onClick={handleNavMobile}
            >
              <TabNav />
            </Button>
          </div>
        </div>
      </div>
      <Drawer
        title={
          <div className="drawerNav">
            <div className="dFlex">
              <img className="logoNavMobile" src={logo} alt="img" />
              <div className="dFlexColumnJustifyCenter">
                <span className="textLogo semibold">
                  LIÊN ĐOÀN BÓNG CHUYỀN VIỆT NAM
                </span>
                <span className="textLogo">
                  Volleyball Federation of Vietnam
                </span>
              </div>
            </div>
            <Button
                type="primary"
                className="flexCenter buttonNav"
                ghost={true}
                onClick={onCloseDrawer}
            >
              <CloseNav />
            </Button>
          </div>
        }
        closable={false}
        width={"100%"}
        onClose={onCloseDrawer}
        visible={openNav}
      >
        <div className="navMobile">
          {HEADERNAV.map((item: any) => {
            return (
                <Link to={item.link} key={item.key}>
                  <Button
                      type="primary"
                      icon={pathname === item.link ? <Volleyball /> : false}
                      className="flexCenter buttonNav"
                      ghost={true}
                  >
                    <span
                        className={`textNav ${
                            pathname === item.link ? "active" : ""
                        }`}
                    >
                      {item.text}
                    </span>
                  </Button>
                </Link>
            );
          })}
        </div>
      </Drawer>
    </header>
  );
};
export default Header;
