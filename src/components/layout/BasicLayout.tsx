import React from "react";
import "antd/dist/antd.css";
import "../../assets/scss/style.scss";
// import "../../assets/style/news-styles.css";
// import "../../assets/style/detail-styles.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Header from "./Header";
import Footer from "./Footer";
import { Spin } from "antd";
import { Loading3QuartersOutlined } from "@ant-design/icons/lib";
import {useSelector} from "react-redux";
interface Props {
  children: any;
}
const BasicLayout: React.FC<Props> = ({ children }) => {
  const { isShowLoading } = useSelector((state: any) => {
    return {
      isShowLoading: state.commonReducer.isShowLoading || false,
    };
  });

  return (
    <div className={"pageBody"}>
      <Spin
        spinning={isShowLoading}
        indicator={<Loading3QuartersOutlined spin style={{ fontSize: 25 }} />}
      >
        <Header />

        <main className="main">{children}</main>

        <Footer />
      </Spin>
    </div>
  );
};

export default BasicLayout;
