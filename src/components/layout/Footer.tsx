// @ts-ignore
import React from 'react';
// @ts-ignore
import logo from '../../assets/image/image 2.png'
import { Col, Row } from 'antd'
// @ts-ignore
import background from "../../assets/image/BottomLine-bg.png";
const Footer = () =>  {
        return (
            <footer>
                <div className="footer flexCenter">
                    <Row gutter={20} className="container">
                        <Col xs={24} sm={24} md={8}>
                            <div className="dFlexColumnAlignCenter contentLogo">
                                <img src={logo} alt="img" />
                                <div className="dFlexColumnAlignCenter">
                                    <span className="textLogo semibold">LIÊN ĐOÀN BÓNG CHUYỀN VIỆT NAM</span>
                                    <span className="textLogo">Volleyball Federation of Vietnam</span>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={8} className="footerTitle">
                            <Row gutter={20} className="dFlex">
                                <Col span={12} className="dFlexColumn">
                                    <p className="text-header semibold">Địa chỉ</p>
                                    <p className="text-body">Tầng 5, Nhà E, 36 Trần Phú, Ba Đình, Hà Nội</p>
                                </Col>
                                <Col span={12} className="dFlexColumn">
                                    <p className="text-header semibold">Email</p>
                                    <p className="text-body">volleyvn@gmail.com</p>
                                </Col>
                            </Row>
                            <Row gutter={20} className="dFlex">
                                <Col span={12} className="dFlexColumn" style={{ marginTop: 24 }}>
                                    <p className="text-header semibold">Điện thoại</p>
                                    <p className="text-body">+84 24 3823 4555</p>
                                </Col>
                                <Col span={12} className="dFlexColumn" style={{ marginTop: 24 }}>
                                    <p className="text-header semibold">Fax</p>
                                    <p className="text-body">+84 24 3843 3915</p>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={12} sm={12} md={4}>
                            <div className="dFlexColumn footerTitle">
                                <p>Tin tức</p>
                                <p>Giải đấu</p>
                                <p>Đội bóng</p>
                                <p>Thư viện ảnh</p>
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={4}>
                            <div className="dFlexColumn footerTitle">
                                <p>Chính sách bảo mật</p>
                                <p>Điều khoản sử dụng</p>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className='copyright' style={{background: `url(${background})` }}>
                    <span className="white-1" style={{padding: 14}}>ⓒ Copyright 2022 by VFV</span>
                </div>
            </footer>
        );

}

export default Footer;