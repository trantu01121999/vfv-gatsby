import React, { Component } from "react";
// @ts-ignore
import image13 from "../../assets/image/image 13.png";
const AboutUsHistory = () => {
  return (
    <div className={"aboutUsHistory"}>
      <div className="image">
        <img src={image13} alt="" />
      </div>
      <div className="text">
        <p>
          Bóng chuyền (BC) bắt đầu du nhập vào nước ta qua các lái buôn người Ấn
          Độ, Trung Hoa, Pháp, sau đó là những thành viên của bộ máy cai trị của
          thực dân Pháp. Sau Cách mạng tháng Tám, hưởng ứng lời kêu gọi toàn dân
          tập thể dục của Bác Hồ, BC được phổ cập rộng rãi hơn và trở thành một
          môn thể thao chủ yếu được tập luyện của các đơn vị bộ đội, cơ quan,
          trường học.
        </p>
        <p>
          Năm 1957, đội tuyển BC nam nước Việt Nam Dân chủ Cộng hòa chính thức
          ra đời. Trên cơ sở đó, hình thành các giải đấu chuyên nghiệp. Cho đến
          năm 1991, Liên đoàn bóng chuyền Việt Nam (VFV) được thành lập. Từ đó,
          môn BC được phát triển ngày càng vững chắc, gặt hái nhiều thành công,
          hệ thống thi đấu bắt đầu đi vào nền nếp và ổn định.
        </p>
        <p>
          {" "}
          Giai đoạn về sau chứng kiến BCVN phát triển mạnh mẽ nhất nhờ hệ thống
          tổ chức thi đấu ổn định; đội ngũ HLV, VĐV và trọng tài được đào tạo
          chính quy và bài bản cùng với sự thành công của hệ thống các Trung tâm
          đào tạo VĐV trẻ. Một số giải quốc tế đã được đăng cai tổ chức khá
          thành công, kể cả BC trong nhà và BC bãi biển: SEA Games 22, Cúp Hùng
          Vương, Giải bóng chuyền quốc tế mở rộng Cúp VTV Bình Điền, Cúp
          VIETSOVPETRO, ...
        </p>
        <p>
          {" "}
          VFV cũng đã phối hợp chặt chẽ với các đơn vị, tổ chức được nhiều hoạt
          động, giải đấu quan trọng; tham gia các Đại hội thể thao quốc tế lớn
          như Giải Vô địch Bóng chuyền nữ Châu Á 2017, 2019; ASIAD 2018; SEA
          Games 2017, 2019…
        </p>
        <p>
          {" "}
          Trong thời gian tới, Liên đoàn Bóng chuyền Việt Nam sẽ tiếp tục xây
          dựng khung pháp lý, cải thiện hệ thống đào tạo và tăng cường tuyên
          truyền, quảng bá hình ảnh để bóng chuyền Việt Nam ngày càng phát
          triển.
        </p>
      </div>
    </div>
  );
};

export default AboutUsHistory;
