import React, { Component } from "react";
// @ts-ignore
import image12 from "../../assets/image/image 12.png";
const AboutUsContent = () => {
    return (
        <div className={"aboutUsContent"}>

            <div className="image">
                <img src={image12} alt="" />
            </div>
            <div className="text">
                <p>
                    Hiệp hội bóng chuyền nước Việt Nam dân chủ cộng hoà (nay gọi là Liên
                    đoàn Bóng chuyền Việt Nam) được thành lập ngày 10 tháng 6 năm 1961
                    theo Quyết định số 138-NV ngày 10/6/1961 của Bộ trưởng Bộ Nội vụ.
                </p>
                <p>
                    {" "}
                    Trải qua hơn 50 năm xây dựng và phát triển với nhiều khó khăn, thách
                    thức, Liên đoàn Bóng chuyền Việt Nam đã từng bước lớn mạnh và trở
                    thành một trong những tổ chức xã hội - nghề nghiệp uy tín của Thể thao
                    Việt Nam. Liên đoàn bóng chuyền Việt Nam được tín nhiệm trao quyền
                    đăng cai, tổ chức thành công nhiều giải bóng chuyền của Châu lục và
                    Thế giới, góp phần tích cực tăng cường tình hữu nghị, đoàn kết gắn bó
                    với các quốc gia trên thế giới, đóng góp đáng kể vào thành công chung
                    của thể thao Việt Nam.
                </p>
            </div>
        </div>
    );
};

export default AboutUsContent;
