import React, { Component } from "react";
// @ts-ignore
import image12 from "../../assets/image/image12.png";
const AboutUsMandate = () => {
  return (
    <div className={"aboutUsMandate"}>
      <div className="image">
        <img src={image12} alt="" />
      </div>
      <div className="text">
        <p>
          Nhiệm vụ của Liên đoàn bóng chuyền Việt Nam bao gồm:
        </p>
        <ul>
          <li>
            {" "}
            Phát triển phong trào tập luyện bóng chuyền cho mọi đối tượng quần
            chúng
          </li>
          <li> Xây dựng và hoàn thiện hệ thống thi đấu các giải quốc gia</li>
          <li>
            {" "}
            Phát triển kế hoạch hợp tác quốc tế về bóng chuyền, với LĐBC Thế
            giới, LĐBC Châu Á, LĐBC Đông Nam Á…
          </li>
          <li>
            {" "}
            Tổ chức và quản lý các giải đấu bóng chuyền trong nước và quốc tế
            tại Việt Nam.
          </li>
          <li>
            {" "}
            Xây dựng nền bóng chuyền lành mạnh, ngăn chặn tiêu cực, chống tham
            nhũng và sử dụng chất kích thích.
          </li>
          <li> Phát tiển, khuyến khích các giải bóng chuyền ở địa phương.</li>
          <li>
            {" "}
            Thu hút các nguồn tài trợ trong và ngoài nước, huy động các nguồn
            lực xã hội để xây dựng cơ sở vật chất cho hoạt động bóng chuyền
          </li>
          <li>
            {" "}
            Đề xuất với cơ quan quản lý nhà nước về chủ trương, chính sách, kế
            hoạch, biện pháp nhằm nâng cao trình độ, xây dựng chiến lược phát
            triển môn bóng chuyền.
          </li>
        </ul>
      </div>
    </div>
  );
};

export default AboutUsMandate;
