import React  from "react";
// @ts-ignore
import organizationalStructure from "../../assets/image/so-do-to-chuc.png";
const OrganizationalStructure = () => {
    return (
        <div className={"organizationalStructure"}>
            <div className="image">
                <img src={organizationalStructure} alt="" />
            </div>
        </div>
    );
};

export default OrganizationalStructure;
