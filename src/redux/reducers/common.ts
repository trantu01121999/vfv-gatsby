import {getUserInfo} from "../../utils/common";

const userSession = getUserInfo()
const commonReducer = (state = {}, action: { type: string; payload: any }) => {
    switch (action.type) {
        case "SHOW_LOADING" :
            return { ...state, isShowLoading: action.payload }
        default:
            return { ...state, isLoggedIn: !!userSession }
    }
}

export default commonReducer
