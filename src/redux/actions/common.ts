export const toggleShowLoading = (isLoading: boolean) => ({
    type: "SHOW_LOADING",
    payload: isLoading
})
