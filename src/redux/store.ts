import { applyMiddleware, createStore } from "redux"
import rootReducer from './root-reducers'
import thunk from 'redux-thunk';

const store = createStore(rootReducer, applyMiddleware(thunk))
export default store
