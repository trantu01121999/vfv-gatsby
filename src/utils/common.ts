
export const getUserInfo = () => {
    const user = typeof window !== 'undefined' && window.localStorage.getItem('userInfo')
    if (user) {
        return JSON.parse(user)
    }
    return null
}