// @ts-ignore
import React, { Component } from "react";
import BasicLayout from "../components/layout/BasicLayout";
import { TABS, NEW } from "../contants/common";
// @ts-ignore
import logoGiaiDau from "../assets/images/logo-giai-dau.png";
// @ts-ignore
import image2 from "../assets/images/Classification_tag.png";
// @ts-ignore
import banner from "../assets/image/cover-bg.png";
import { Breadcrumb } from "antd";
import { Tabs } from 'antd';

const { TabPane } = Tabs;

const GiaiDauDetail = () => {
    function callback(key) {
      console.log(key);
    }
  return (
      <BasicLayout>
        <div className="tournaments-detail">
            <div className="banner" style={{background: `url(${banner})`}}>
                <div className="top-detail">
                    <img src={logoGiaiDau} alt="" />
                    <div className="breadcrumb-top">
                        <Breadcrumb separator=">">
                            <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
                            <Breadcrumb.Item href="/gioi-thieu">Giải đấu</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Giải đấu 2022</Breadcrumb.Item>
                        </Breadcrumb>
                        <h2>Giải bóng chuyền VĐQG Cúp Bamboo Airways năm 2022</h2>
                    </div>
                </div>
            </div>
            <div className="container">
            <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Tổng quan" key="1">
                    <div className="tab1">
                        <div className="tab1-top">
                            <div className="tab1-top-row">
                                <div className="tab1-top-1">
                                    <p>Thời gian</p>
                                    <p>18/4/2022 - 21/4/2022</p>
                                </div>
                                <div className="tab1-top-1">
                                    <p>Nội dung</p>
                                    <p>Nam, Nữ</p>
                                </div>
                                <div className="tab1-top-1">
                                    <p>Địa điểm</p>
                                    <p>Bảng A: Nhà thi đấu tỉnh Phú Thọ </p>
                                    <p>Bảng B: Nhà thi đấu tỉnh Hải Dương</p>
                                </div>
                            </div>
                            <div className="tab1-top-row">
                                <div className="tab1-top-1">
                                    <p>Tổng giải thưởng</p>
                                    <p>1.000.000.000 VNĐ</p>
                                </div>
                                <div className="tab1-top-1">
                                    <p>Số trận đấu</p>
                                    <p>40</p>
                                </div>
                                <div className="tab1-top-1">
                                    <p>Số lượng đội tham dự</p>
                                    <p>12 </p>
                                </div>
                            </div>
                        </div>
                        <p className="text-bottom">Giải vô địch bóng chuyền quốc gia Việt Nam (xuất phát là Giải vô địch bóng chuyền các đội mạnh toàn quốc phát triển lên chuyên nghiệp) là giải thi đấu bóng chuyền cao nhất trong hệ thống bóng chuyền Việt Nam. Giải do Liên đoàn bóng chuyền Việt Nam tổ chức tính từ năm 2004 đến mùa giải năm 2021 đã có 18 lần được tổ chức. Hai đội Nam Câu lạc bộ bóng chuyền Tràng An Ninh Bình và Nữ Câu lạc bộ bóng chuyền Bộ Tư lệnh Thông tin - FLC đang là đương kim vô địch của giải. Giải bóng chuyền vô địch quốc gia Việt Nam 2022 dự kiến diễn ra tháng 7/2022 tại Vĩnh Phúc và Ninh Bình.</p>
                    </div>
                </TabPane>
                <TabPane tab="Thể thức thi đấu" key="2">
                    <div className="tab1">
                        <p>
                            Từ năm 2004 đến năm 2014, mỗi mùa giải có 12 câu lạc bộ bóng chuyền nam và 12 câu lạc bộ bóng chuyền nữ tham dự và tạo ra 2 bảng nam và 2 bảng nữ mỗi bảng 6 đội. Giai đoạn 2015-2017 có bảng chỉ có 5 đội. Kết quả bốc thăm chia bảng giữ nguyên cho cả hai giai đoạn thi đấu. Ở giai đoạn 1, các đội trong bảng sẽ thi đấu vòng tròn 1 lượt tính điểm, các đội ở tốp đầu sẽ lọt vào vòng chung kết để tranh chức vô địch lượt đi (Cúp Hoa Lư, Cúp Hùng Vương). Vòng chung kết này không tính điểm cho giai đoạn sau. Vòng thi đấu lượt về cũng thi đấu vòng tròn 1 lượt, cách tính điểm có gộp với giai đoạn lượt đi. Các đội đạt thành tích cao sẽ vào vòng tranh cúp vô địch còn các đội nằm ở tốp cuối mỗi bảng sẽ thi đấu vòng chung kết ngược để chọn 2 đội xuống hạng. Chung kết ngược nam và nữ đều có 4 đội tham gia, 2 đội phải xuống hạng.
                        </p>
                        <p>
                            Từ năm 2015 trở đi cách tính điểm và xếp hạng có thay đổi khi đội thua vẫn có thể được điểm: Trận thắng với tỷ số (3-0 hoặc 3-1): đội thắng được 3 điểm, đội thua được 0 điểm. Trận thắng với tỷ số (3-2): đội thắng được 2 điểm, đội thua được 1 điểm. Bỏ cuộc: 0 điểm. Xếp hạng: Đội có nhiều trận thắng nhất xếp trên. Nếu hai hay nhiều đội có tổng số trận thắng bằng nhau thì Đội nào có tổng số điểm nhiều hơn thì xếp trên. Trong trường hợp hai hay nhiều đội có tổng số điểm bằng nhau thì đội nào có tỷ số "tổng hiệp thắng/tổng hiệp thua" lớn hơn đội đó xếp trên. Nếu tỷ số "tổng hiệp thắng/tổng hiệp thua" vẫn bằng nhau thì đội nào có "tổng quả thắng/tổng quả thua" lớn hơn sẽ xếp trên. Nếu tỷ số "tổng quả thắng/tổng quả thua" vẫn bằng nhau thì đội nào thắng trong trận đấu giữa 02 đội ở vòng II xếp trên.
                        </p>
                        <p>
                            Từ mùa giải 2018, số lượng đội bóng tham dự mỗi mùa giải rút xuống còn 10 đội ở mỗi nội dung.
                        </p>
                        <p>
                            Từ mùa giải 2019 trở đi, Sau khi vòng I khép lại, các đội xếp ở vị trí 1, 3 và 5 của bảng A và B sẽ lần lượt nhóm lại với các đội xếp hạng 2 và 4 ở bảng B và A để tạo thành bảng C và D. Các đội sẽ tiếp tục thi đấu vòng tròn một lượt tính điểm, sau đó cộng tổng thành tích ở hai vòng bảng để tìm ra những cái tên lọt vào vòng chung kết.[1] Tại vòng chung kết và xếp hạng, các đội nhất, nhì của bảng C, D (nam, nữ) thi đấu bán kết (nhất C gặp nhì D và nhất D gặp nhì C). Hai đội thắng bán kết tranh nhất nhì, hai đội thua ở bán kết tranh hạng ba. Các đội xếp hạng 3 của hai bảng C, D thi đấu xếp hạng 5, 6. Các đội thứ 4, 5 của hai bảng C, D đấu chéo chia hạng 7, 8, 9 và 10.[2]
                        </p>
                        <p>
                            Từ mùa giải 2021 trở đi, sau khi các đội bóng chuyền hoàn thành 2 vòng đấu, kết quả thi đấu được tổng hợp và xếp hạng từ 1-10 (theo các nội dung nam, nữ). Bước vào bán kết sẽ là hai cặp đấu 1 vs 4 và 2 vs 3 tính theo bảng xếp hạng. Vòng đấu tranh vé trụ hạng sẽ là hai cặp đấu 7 vs 10 và 8 vs 9. Hai đội thua bước vào trận chung kết ngược để tìm ra đội bóng phải xuống chơi tại giải hạng A mùa giải sau.[3] Tuy nhiên, đến khi bước vào giai đoạn 2 thì hai đội bóng Long An bỏ cuộc do dịch Covid nên điều lệ giải lại quay lại giống năm 2020.
                        </p>
                    </div>
                </TabPane>
                <TabPane tab="Các đội tham dự" key="3">
                    <div className="tab1">
                    {NEW.map((item: any, index: number) => {
                        return (
                            <div className="tab1-col2 flexCenter2">
                                <div className="tab1-col2-col">
                                    <p>Đội Nam</p>
                                </div>
                                <div className="tab1-col2-col">
                                    <p>Đội Nữ</p>
                                </div>
                            </div>
                        );
                    })}
                    </div>
                </TabPane>
                <TabPane tab="Lịch thi đấu & kết quả" key="4">
                    <div className="tab1">
                    <p className="date">18/04/2022</p>
                    {NEW.map((item: any, index: number) => {
                        return (
                            <div className="tab1-col2 flexCenter2">
                                <div className="tab1-col2-col flexCenter2">
                                    <img src={image2} alt="" />
                                    <div className="right-img flexCenter">
                                        <ul className="dFlex">
                                            <li>Trận 1</li>
                                            <li>Vòng Bảng</li>
                                            <li>Bảng B</li>
                                        </ul>
                                        <div className="right-img-bottom">
                                            <div className="right-img-bottom-1">
                                                <p>Tràng An Ninh Bình</p>
                                                <p className="score">3 - 1</p>
                                                <p>Thành phố Hồ Chí Minh</p>
                                            </div>
                                            <div className="right-img-bottom-2">
                                                <p>(25/21, 25/18, 26/28, 25/18)</p>
                                            </div>
                                            <div className="right-img-bottom-3">
                                                <p>15h00, 18/04/2022</p>
                                                <p>Hải Dương</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab1-col2-col flexCenter2">
                                    <img src={image2} alt="" />
                                    <div className="right-img flexCenter">
                                        <ul className="dFlex">
                                            <li>Trận 1</li>
                                            <li>Vòng Bảng</li>
                                            <li>Bảng B</li>
                                        </ul>
                                        <div className="right-img-bottom">
                                            <div className="right-img-bottom-1">
                                                <p>Tràng An Ninh Bình</p>
                                                <p className="score">3 - 1</p>
                                                <p>Thành phố Hồ Chí Minh</p>
                                            </div>
                                            <div className="right-img-bottom-2">
                                                <p>(25/21, 25/18, 26/28, 25/18)</p>
                                            </div>
                                            <div className="right-img-bottom-3">
                                                <p>15h00, 18/04/2022</p>
                                                <p>Hải Dương</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                    </div>
                </TabPane>
            </Tabs>
            </div>
        </div>
      </BasicLayout>
  );
};

export default GiaiDauDetail;
