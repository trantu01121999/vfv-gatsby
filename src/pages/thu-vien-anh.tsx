// @ts-ignore
import React from "react";
// @ts-ignore
import slide from "../assets/image/Rectangle 15.png";
import BasicLayout from "../components/layout/BasicLayout";
import { NEW } from "../contants/common";

const LibraryImage = () => {
  return (
    <BasicLayout>
      <div className={"pageAlbum container"}>
        <div className="news">
          {NEW.map(() => {
            return (
              <div className="new">
                <div className="new-content-img">
                    <img style={{ height: 224 }} src={slide} alt="img" />
                </div>
                <div className="new-content">
                  <p className="semibold mt-10 size-16">
                    Thắng Ninh Bình Doveco 3-2, Than Quảng Ninh giành hạng 3
                    giải bóng chuyền VĐQG Cúp Bamboo Airways 2021
                  </p>
                  <span style={{ color: "#8A8F8C" }} className="size-16">
                    40 ảnh
                  </span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </BasicLayout>
  );
};

export default LibraryImage;
