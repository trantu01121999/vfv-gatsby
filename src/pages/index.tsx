// @ts-ignore
import React, { useEffect } from "react";
import { Carousel, Col, Row } from "antd";
import { useDispatch } from "react-redux";
import { toggleShowLoading } from "../redux/actions/common";
import BasicLayout from "../components/layout/BasicLayout";
import { NEW, SLIDE } from "../contants/common";
// @ts-ignore
import slide from "../assets/image/Rectangle 15.png";
// @ts-ignore
import bambo from "../assets/image/image 3.png";
// @ts-ignore
import flc from "../assets/image/image 4.png";
// @ts-ignore
import gas from "../assets/image/image 5.png";
// @ts-ignore
import dongluc from "../assets/image/image 6.png";
// @ts-ignore
import mikasa from "../assets/image/image 7.png";
// @ts-ignore
import vtv from "../assets/image/image 8.png";
// @ts-ignore
import fivb from "../assets/image/image 9.png";
// @ts-ignore
import avc from "../assets/image/image 10.png";
// @ts-ignore
import web from "../assets/image/image 11.png";
import { Ring } from "../contants/icon";

const Index = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(toggleShowLoading(false));
  }, []);
  const changeSlide = () => {};

  return (
    <BasicLayout>
      <main className="main container">
        <div className="main-top">
          <Row gutter={32}>
            <Col xs={24} sm={24} md={16}>
              <Carousel className="slide" autoplay afterChange={changeSlide}>
                {SLIDE.map(() => {
                  return (
                    <div className="slide-main">
                      <div className="slide-bg"></div>
                      <div className="slide-content">
                        <div className="slide-content-top">
                          <div className="new mr-10">
                            <span>Giải đấu trong nước</span>
                          </div>
                          <span>15/01/2022</span>
                        </div>
                        <div className="slide-content-bot">
                          <span>
                            BTL Thông Tin FLC bảo vệ thành công ngôi vô địch
                            giải bóng chuyền VĐQG Cúp Bamboo Airways 2021
                          </span>
                        </div>
                      </div>
                      <img src={slide} alt="img" />
                    </div>
                  );
                })}
              </Carousel>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <div className="noti">
                <div className="noti-header flexGroup7">
                  <Ring />
                  <span>THÔNG BÁO</span>
                </div>
                <div className="noti-body">
                  <div className="noti-body-content">
                    <p className="semibold size-16">
                      Liên đoàn Bóng chuyền Việt Nam thông báo không tổ chức một
                      số giải bóng chuyền năm 2021
                    </p>
                    <span style={{ color: "#8A8F8C" }}>14/01/2022</span>
                  </div>
                  <div className="noti-body-content">
                    <p className="semibold size-16">
                      Liên đoàn Bóng chuyền Việt Nam thông báo không tổ chức một
                      số giải bóng chuyền năm 2021
                    </p>
                    <span style={{ color: "#8A8F8C" }}>14/01/2022</span>
                  </div>
                  <div className="noti-body-content">
                    <p className="semibold size-16">
                      Liên đoàn Bóng chuyền Việt Nam thông báo không tổ chức một
                      số giải bóng chuyền năm 2021
                    </p>
                    <span style={{ color: "#8A8F8C" }}>14/01/2022</span>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="main-middle">
          <span className="semibold size-20 title">
            TIN TỨC SỰ KIỆN MỚI NHẤT
          </span>
          <div className="news">
            {NEW.map(() => {
              return (
                <div className="new">
                  <img style={{ height: 224 }} src={slide} alt="img" />
                  <div className="new-content">
                    <div className="tag">
                      <span>Giải đấu trong nước</span>
                    </div>
                    <p className="semibold mt-10 size-16">
                      Thắng Ninh Bình Doveco 3-2, Than Quảng Ninh giành hạng 3
                      giải bóng chuyền VĐQG Cúp Bamboo Airways 2021
                    </p>
                    <span style={{ color: "#8A8F8C" }} className="size-16">
                      14/01/2022
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </main>
      <div className="main-bot">
        <div className="flexCenter">
            <span
                className="semibold size-20"
                style={{ color: "#16221B", paddingBottom: 30, paddingTop: 30 }}
            >
              CÁC ĐỐI TÁC VÀ NHÀ TÀI TRỢ
            </span>
        </div>
        <div className="flexCenter main-bot-partner">
          <img src={bambo} alt="img" />
          <img src={flc} alt="img" />
          <img src={gas} alt="img" />
          <img src={dongluc} alt="img" />
          <img src={mikasa} alt="img" />
        </div>
        <div className="flexCenter main-bot-partner">
          <img src={vtv} alt="img" />
          <img src={fivb} alt="img" />
          <img src={avc} alt="img" />
          <img src={web} alt="img" />
        </div>
        <div className="main-bot-partner-mobile">
          <img
              src={bambo}
              alt="img"
              width={"100%"}
              style={{
                paddingLeft: 44,
                paddingRight: 44,
                paddingTop: 16,
                paddingBottom: 16,
              }}
          />
          <div className="imgs">
            <img src={flc} alt="img" />
            <img
                src={gas}
                alt="img"
                style={{ paddingLeft: 44, paddingRight: 44 }}
            />
            <img src={dongluc} alt="img" />
            <img src={mikasa} alt="img" />
            <img src={vtv} alt="img" />
            <img src={fivb} alt="img" />
            <img src={avc} alt="img" />
            <img src={web} alt="img" />
          </div>
        </div>
      </div>
    </BasicLayout>
  );
};

export default Index;
