// @ts-ignore
import React, { useEffect, useState } from "react";
import BasicLayout from "../components/layout/BasicLayout";
// @ts-ignore
import banner from "../assets/image/cover-bg.png";
import { Breadcrumb } from "antd";
import AboutUsHistory from "../components/about-us/AboutUsHistory";
import AboutUsContent from "../components/about-us/AboutUsContent";
import AboutUsMandate from "../components/about-us/AboutUsMandate";
import OrganizationalStructure from "../components/about-us/OrganizationalStructure";
import { useLocation } from "@reach/router";
import * as queryString from "query-string";
const AboutUs = () => {
  const { pathname, search } = useLocation();
  const [menuType, setMenuType] = useState<string>("");
  useEffect(() => {
    const query: any = queryString.parse(search);
  }, []);
  const handleChangeMenu = (value: string) => {
    setMenuType(value)
  }
  return (
    <BasicLayout>
      <div className={"pageAboutUs"}>
        <div className="banner">
          <img src={banner} alt="" />
          <div className="text">
            <Breadcrumb separator=">">
              <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item href="/gioi-thieu">Giới thiệu</Breadcrumb.Item>
            </Breadcrumb>
            <h2>Giới thiệu liên đoàn</h2>
          </div>
        </div>
        <div className="menu">
          <div onClick={()=>handleChangeMenu('history')} className={`menuItem ${menuType === 'history' ? 'active': ''}`}>
            <span>Lịch sử phát triển</span>
          </div>
          <div onClick={()=>handleChangeMenu('mandate')} className={`menuItem ${menuType === 'mandate' ? 'active': ''}`}>
            <span>Chức năng nhiệm vụ</span>
          </div>
          <div onClick={()=>handleChangeMenu('structure')} className={`menuItem ${menuType === 'structure' ? 'active': ''}`}>
            <span>Cơ cấu tổ chức</span>
          </div>
        </div>
        <div className="content">
          <div className="container">
            {menuType === "" && <AboutUsContent />}
            {menuType === "history" && <AboutUsHistory />}
            {menuType === "mandate" && <AboutUsMandate />}
            {menuType === "structure" && <OrganizationalStructure />}
          </div>
        </div>
      </div>
    </BasicLayout>
  );
};

export default AboutUs;
