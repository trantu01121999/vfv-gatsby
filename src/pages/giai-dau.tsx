// @ts-ignore
import React  from "react";
import Slider from "react-slick";
import BasicLayout from "../components/layout/BasicLayout";
import { YEAR, NEW } from "../contants/common";
// @ts-ignore
import imageGiaiDau from "../assets/images/giaidau.png";

const GiaiDau = () => {
  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 15,
    slidesToScroll: 15
  };
  return (
      <BasicLayout>
        <div className="white-top tournaments">
          <div className="top">
            <div className="slider">
              <Slider {...settings}>
                  {YEAR.map((item: any, index: number) => {
                    return (
                      <div>
                        <h3>{item}</h3>
                      </div>
                    );
                  })}
              </Slider>
            </div>
            <div className="main-title">
                <h3>CÁC GIẢI ĐẤU TRONG NĂM 2022</h3>
            </div>
          </div>
        </div>
        <div className="container tournaments">
          <div className="flexGroup5 flexCenter2">
            {NEW.map((item: any, index: number) => {
              return (
                <div className="box-info">
                  <a href="#">
                    <div className="box-info-top">
                      <img src={imageGiaiDau} alt="" />
                      <div className="box-info-top-time">
                        <p>Thời gian</p>
                        <p>18/4/2022 - 21/4/2022</p>
                      </div>
                    </div>
                    <div className="box-info-bottom">
                        <p>Giải bóng chuyền hạng A toàn quốc Cúp FLC năm 2021</p>
                    </div>
                  </a>
                </div>
              );
            })}
          </div>
        </div>
      </BasicLayout>
  );
};

export default GiaiDau;
