// @ts-ignore
import React, { Component } from "react";
import BasicLayout from "../components/layout/BasicLayout";
// @ts-ignore
import itemClub from "../assets/image/item-club.png";
import { SLIDE } from "../contants/common";

const Club = () => {
  return (
    <BasicLayout>
      <div className={"pageClub"}>
        <div className="pageClubMenu">
          <div className="container">
            <h2 className={"title"}>CÁC CÂU LẠC BỘ BÓNG CHUYỀN VIỆT NAM</h2>
            <div className="menu">
              <div className="menuItem">
                <span>Câu lạc bộ Nam</span>
              </div>
              <div className="menuItem">
                <span>Câu lạc bộ Nữ</span>
              </div>
            </div>
          </div>
        </div>
        <div className="pageClubContent">
          <div className="container">
            <div className="listClub">
              {SLIDE.map((item: any, index: number) => {
                return (
                  <div className="itemClub" key={index}>
                    <div className="image">
                      <img src={itemClub} alt="" />
                    </div>
                    <div className="text">
                      <div className="nameClub">Thể Công</div>
                      <div className="description">
                        Câu lạc bộ bóng chuyền nam Thể Công
                      </div>
                      <div className="info">
                        <div className="itemInfo">
                          <span>Thành lập</span>
                          <p>1994</p>
                        </div>
                        <div className="itemInfo">
                          <span>Thành phố</span>
                          <p>Hà Nội</p>
                        </div>
                        <div className="itemInfo">
                          <span>Sân nhà</span>
                          <p>Nhà thi đấu Quần Ngựa</p>
                        </div>
                        <div className="itemInfo">
                          <span>Huyến luyện viên</span>
                          <p>Thái Anh An</p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </BasicLayout>
  );
};

export default Club;
