// @ts-ignore
import React, { useState } from "react";
// @ts-ignore
import moment from "moment";
// @ts-ignore
import image1 from "../../assets/image/khi-dau-vn.png";
// @ts-ignore
import detail1 from "../../assets/image/new-detail.png";
import BasicLayout from "../../components/layout/BasicLayout";
import { Breadcrumb, Tag } from "antd";
import { NEW } from "../../contants/common";
import { Link } from "gatsby";
import ItemNew from "../../components/common/ItemNew";

interface Props {}

const NewDetail: React.FC<Props> = () => {
  const [menu, setMenu] = useState<string>("all");
  const handleChangeMenu = (value: string) => {
    setMenu(value);
  };
  return (
    <BasicLayout>
      <section className="pageNewDetail">
        <div className="container">
          <div className="pageNewDetailContent">
            <div className="contentLeft">
              <img src={image1} alt="" />
            </div>
            <div className="contentRight">
              <Breadcrumb separator=">">
                <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
                <Breadcrumb.Item>Tin tức</Breadcrumb.Item>
                <Breadcrumb.Item>
                  Lịch thi đấu bóng chuyền trong nhà và bóng chuyền bãi biển SEA
                  ...
                </Breadcrumb.Item>
              </Breadcrumb>
              <div className="infoNew">
                <div className="tag">
                  <Tag color="#00923F">Giải đấu trong nước</Tag>
                </div>
                <div className="time">{moment().format("DD/MM/YYYY")}</div>
              </div>
              <div className="content">
                <h1 className={"title"}>
                  Lịch thi đấu bóng chuyền trong nhà và bóng chuyền bãi biển SEA
                  Games 31
                </h1>
                <h2>
                  Tổng cục Thể dục Thể thao đã ký quyết định ban hành Lịch thi
                  đấu SEA Games 31 với 40 bộ môn trong đó có nội dung bóng
                  chuyền bãi biển và bóng chuyền trong nhà.
                </h2>
                <p>
                  Sau khi buộc phải hoãn vì dịch bệnh, SEA Games 31 sẽ chính
                  thức khởi tranh ngày 6 - 23/5/2022 với 40 bộ môn thi đấu tại
                  Hà Nội và 11 tỉnh thành lân cận.
                </p>
                <p>
                  Ở nội dung bóng chuyền trong nhà, các trận đấu nội dung nam/nữ
                  diễn ra từ 13 - 22/5 tại nhà thi đấu đa năng Đại Yên, thành
                  phố Hạ Long tỉnh Quảng Ninh.
                </p>
                <img src={detail1} alt="" />
                <p>
                  Nhà thi đấu Thể thao đa năng Đại Yên với sức chứa 5.000 chỗ
                  ngồi được xây dựng gồm 3 tầng với chiều cao 24,4m. Trong đó
                  tầng 1 gồm khu vực thi đấu, khu vực phụ trợ cho trọng tài,
                  VĐV, HLV.
                </p>
                <p>
                  Tầng 2 và tầng 3 là khu khán đài, chỗ ngồi thường, chỗ ngồi
                  VIP, phòng truyền thông, phòng tường thuật, phòng hội thảo và
                  những khu chức năng.
                </p>
                <p>
                  Nội dung bóng chuyền bãi biển nam/nữ được tổ chức từ 15 - 20/5
                  tại khu vực đảo Tuần Châu tỉnh Quảng Ninh. Ngoài 2 nội dung
                  bóng chuyền trong nhà và bóng chuyền bãi biển, Quảng Ninh còn
                  tổ chức những môn thể thao khác như bóng ném bãi biển, cờ
                  tướng, cờ vua, bóng đá nữ, ba môn và hai môn phối hợp.
                </p>
                <p>
                  Nhà thi đấu Thể thao đa năng Đại Yên với sức chứa 5.000 chỗ
                  ngồi được xây dựng gồm 3 tầng với chiều cao 24,4m. Trong đó
                  tầng 1 gồm khu vực thi đấu, khu vực phụ trợ cho trọng tài,
                  VĐV, HLV.
                </p>
                <p>
                  Tầng 2 và tầng 3 là khu khán đài, chỗ ngồi thường, chỗ ngồi
                  VIP, phòng truyền thông, phòng tường thuật, phòng hội thảo và
                  những khu chức năng.
                </p>
                <p>
                  Nội dung bóng chuyền bãi biển nam/nữ được tổ chức từ 15 - 20/5
                  tại khu vực đảo Tuần Châu tỉnh Quảng Ninh. Ngoài 2 nội dung
                  bóng chuyền trong nhà và bóng chuyền bãi biển, Quảng Ninh còn
                  tổ chức những môn thể thao khác như bóng ném bãi biển, cờ
                  tướng, cờ vua, bóng đá nữ, ba môn và hai môn phối hợp.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className={"newOther"}>
        <div className="container">
          <div className="newOtherContent">
            <div className="listNew">
              {NEW.map((item: any, index: number) => {
                return <ItemNew key={index} />;
              })}
            </div>
          </div>
        </div>
      </section>
    </BasicLayout>
  );
};

export default NewDetail;
