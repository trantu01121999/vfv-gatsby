// @ts-ignore
import React, { useState } from "react";
// @ts-ignore
import image1 from "../../assets/images/Rectangle15.png";
import BasicLayout from "../../components/layout/BasicLayout";
import { NEW } from "../../contants/common";
import { Tag } from "antd";
import {Link} from "gatsby";

interface Props {}

const News: React.FC<Props> = () => {
  const [menu, setMenu] = useState<string>("all");
  const handleChangeMenu = (value: string) => {
    setMenu(value);
  };
  return (
    <BasicLayout>
      <section className="pageNew">
        <div className="container">
          <div className="pageNewContent">
            <div className="contentLeft">
              <h2 className="title-left">Tin tức sự kiện</h2>
              <div className="container-left-bottom">
                <span
                  onClick={() => handleChangeMenu("all")}
                  className={` ${menu === "all" ? "active" : ""}`}
                >
                  Tất cả
                </span>
                <span
                  onClick={() => handleChangeMenu("event")}
                  className={` ${menu === "event" ? "active" : ""}`}
                >
                  Sự kiện giải đấu
                </span>
                <span
                  onClick={() => handleChangeMenu("action")}
                  className={` ${menu === "action" ? "active" : ""}`}
                >
                  Hoạt động liên đoàn
                </span>
                <span
                  onClick={() => handleChangeMenu("notification")}
                  className={` ${menu === "notification" ? "active" : ""}`}
                >
                  Thông báo
                </span>
              </div>
            </div>
            <div className="contentRight">
              <div className="listNew">
                {NEW.map((item: any, index: number) => {
                  return (
                    <div className="itemNew" key={index}>
                      <Link to={'/tin-tuc/giai-dau-trong-nuoc'} className={"itemNewContent"}>
                        <div className="image">
                          <img src={image1} alt="" />
                        </div>
                        <div className="text">
                          <Tag color="#2F3A4A">Giải đấu trong nước</Tag>
                          <div className="box-bottom">
                            <p className="short-desc">
                              Thắng Ninh Bình Doveco 3-2, Than Quảng Ninh giành
                              hạng 3 giải bóng chuyền VĐQG Cúp Bamboo Airwa...
                            </p>
                            <span className="date">14/01/2022</span>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
    </BasicLayout>
  );
};

export default News;
