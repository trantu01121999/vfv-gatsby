FROM node:16-alpine AS build
WORKDIR /app
COPY package*.json ./
RUN npm install
ADD vfv-gatsby /app
RUN npm run build

FROM gatsbyjs/gatsby
COPY --from=build /app/public /pub
