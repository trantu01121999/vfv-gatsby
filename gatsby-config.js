module.exports = {
  siteMetadata: {
      title: `vfv gatsby`,
    siteUrl: `https://www.yourdomain.tld`
  },
  plugins: [
      `gatsby-plugin-netlify-cms`,
      `gatsby-plugin-sass`,
      {
    resolve: 'gatsby-plugin-google-analytics',
    options: {
      "trackingId": "UA-54516992-1"
    }
  }]
};